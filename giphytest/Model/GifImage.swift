//
//  GifImage.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import Foundation

struct GifImage: Codable {
    let url: String?
    let width: String?
    let height: String?
}
