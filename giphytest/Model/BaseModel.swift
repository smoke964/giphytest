//
//  BaseModel.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

protocol BaseModel {
    init?(from data: Data)
}

extension BaseModel {
    static var defaultDecoder: JSONDecoder {
        return .init()
    }
}
