//
//  GifObject.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import Foundation

enum GifObjectType: String, Codable {
    case gif
    case sticker
    case text
}

struct GifObject: Codable {
    let type: GifObjectType
    let id: String
    let slug: String
    let url: String
    let bitlyUrl: String
    let username: String
    let source: String
    let title: String
    let altText: String?
    let images: [String: GifImage]
    
    enum CodingKeys: String, CodingKey {
        case type
        case id
        case slug
        case url
        case username
        case source
        case title
        case images
        case bitlyUrl = "bitly_url"
        case altText = "alt_text"
    }
}
