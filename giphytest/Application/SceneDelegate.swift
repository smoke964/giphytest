//
//  SceneDelegate.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

@available(iOS 13.0, *)
final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        setupWindow(scene: scene)
    }

    func sceneDidDisconnect(_ scene: UIScene) {  }

    func sceneDidBecomeActive(_ scene: UIScene) { }

    func sceneWillResignActive(_ scene: UIScene) { }

    func sceneWillEnterForeground(_ scene: UIScene) { }

    func sceneDidEnterBackground(_ scene: UIScene) { }
}

@available(iOS 13.0, *)
fileprivate extension SceneDelegate {
    func setupWindow(scene: UIScene) {
        let mainModule: MainModule = .init()
        guard let scene = (scene as? UIWindowScene) else { return }
        
        let window: UIWindow = .init(windowScene: scene)
        window.overrideUserInterfaceStyle = .dark
        mainModule.setAsRootView(to: window)
        window.makeKeyAndVisible()
        self.window = window
    }
}
