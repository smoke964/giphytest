//
//  MainViewController.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

protocol MainViewProtocol: ViewProtocol {
    var presenter: MainPresenter! { get set }

    func reloadData(at indexPaths: [IndexPath])
    func removeData()
}

final class MainViewController: UIViewController {
    var presenter: MainPresenter!

    private let kCellId: String = "GifCell"
    private let kCategoryCellId: String = "CategoryCell"
    private let kFooterId: String = "Footer"

    // MARK: - UI

    private let activityIndicator: UIActivityIndicatorView = {
        let style: UIActivityIndicatorView.Style
        if #available(iOS 13.0, *) {
            style = .large
        } else {
            style = .whiteLarge
        }
        let indicator: UIActivityIndicatorView = .init(style: style)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    private lazy var collectionViewLayout: GiphyCollectionViewLayout = {
        let layout: GiphyCollectionViewLayout = .init()
        layout.delegate = self
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        
        let view: UICollectionView = .init(frame: .zero, collectionViewLayout: collectionViewLayout)
        view.contentInsetAdjustmentBehavior = .always
        view.contentInset = .init(top: 4, left: 4, bottom: 4, right: 4)
        view.backgroundColor = .black
        view.dataSource = self
        view.delegate = self
        view.prefetchDataSource = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var categoriesCollectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = .init()
        layout.scrollDirection = .horizontal
        let view: UICollectionView = .init(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .black
        view.contentInset = .init(top: 4, left: 4, bottom: 4, right: 4)
        view.showsHorizontalScrollIndicator = false
        view.delegate = self
        view.dataSource = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return view
    }()

    // MARK: private methods

    private func setupLayout() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = .black
        navigationItem.title = "Giphytest"
        collectionView.register(GifCell.self, forCellWithReuseIdentifier: kCellId)
        categoriesCollectionView.register(CategoryCell.self, forCellWithReuseIdentifier: kCategoryCellId)

        view.addSubview(collectionView)
        view.addSubview(categoriesCollectionView)
        collectionView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()

        NSLayoutConstraint.activate([
            categoriesCollectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            categoriesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            categoriesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: categoriesCollectionView.bottomAnchor, constant: 16),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
    }

    override func viewDidLoad() {
        setupLayout()
        super.viewDidLoad()
        presenter.viewIsReady()
    }
}

extension MainViewController: MainViewProtocol {
    func removeData() {
        activityIndicator.startAnimating()
        collectionView.reloadData()
        collectionViewLayout.clearCache()
        collectionView.scrollToTop(animated: false)
    }
    
    func reloadData(at indexPaths: [IndexPath]) {
        DispatchQueue.main.async { [unowned self] in
            collectionView.performBatchUpdates {
                collectionView.insertItems(at: indexPaths)
                activityIndicator.stopAnimating()
            }
        }
    }
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView === categoriesCollectionView {
            presenter.switchCategory(with: indexPath)
            categoriesCollectionView.reloadData()
        } else {
            let gifObject: GifObject = presenter.gifs[indexPath.item]
            presenter.openGifDetailsModule(gifObject: gifObject)
        }
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = presenter.categories[indexPath.item].rawValue.capitalized.getSize()
        return .init(width: size.width + 32, height: 40)
    }
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView === categoriesCollectionView {
            return presenter.categories.count
        } else {
            return presenter.gifs.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === categoriesCollectionView {
            let cell: CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCategoryCellId, for: indexPath) as! CategoryCell
            let category: GiphyCategory = presenter.categories[indexPath.item]
            let isActive: Bool = category == presenter.currentCategory
            cell.setupCell(category: category.rawValue.capitalized, isActive: isActive)
            return cell
        } else {
            let cell: GifCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellId, for: indexPath) as! GifCell
            cell.setupCellLayout()
            presenter.fetchGif(for: indexPath) { data in
                autoreleasepool {
                    let image: UIImage? = .gifImageWithData(data)
                    DispatchQueue.main.async {
                        cell.setupCell(image: image)
                    }
                }
            }
            return cell
        }
    }
}

extension MainViewController: GiphyCollectionViewLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, cellWidth: CGFloat) -> CGFloat {
        guard let height: Int = Int(presenter.gifs[indexPath.item].images["preview_gif"]?.height ?? "10"),
              let width = Int(presenter.gifs[indexPath.item].images["preview_gif"]?.width ?? "10"),
              let contentWidth: CGFloat = (collectionView.collectionViewLayout as? GiphyCollectionViewLayout)?.contentWidth else {
            return 100
        }
        let ratio: CGFloat = CGFloat(height) / CGFloat(width)
        return ratio * contentWidth
    }
}

extension MainViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let maxItemCount: Int = indexPaths.reduce(0, { max($0, $1.item) })
        if maxItemCount == 40 + (presenter.pageOffset - 1) * 40 {
            print("Prefetch: \(indexPaths)")
            presenter.fetchGifs()
        }
    }
}
