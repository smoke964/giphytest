//
//  MainPresenter.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

typealias GifFetchBlock = (Data) -> Void

final class MainPresenter {
    
    private var kGifLimit: Int = 50
    
    weak var view: MainViewProtocol!
    var giphyService: GiphyServiceProtocol!
    
    var gifs: [GifObject] = []
    var categories: [GiphyCategory] = GiphyCategory.allCases
    var currentCategory: GiphyCategory = .gifs
    var pageOffset: Int = 0
    
    // MARK: - internal methods
    
    func viewIsReady() {
        fetchGifs()
    }
    
    func fetchGifs() {
        giphyService.fetchTrendingGifs(type: currentCategory,
                                       rating: nil, limit: kGifLimit, offset: pageOffset)
        { [unowned self] res in
            gifs += res.data
            view.reloadData(at: calculateIndexPathsToReload())
            pageOffset += 1
        } failure: { [unowned self] error in
            view.showAlert(title: "Attention", message: "Unable to load content, please try later: \(error.localizedDescription)")
        }
    }
    
    func fetchGif(for indexPath: IndexPath, completion: @escaping GifFetchBlock) {
        guard let link = gifs[indexPath.item].images["preview_gif"]?.url else { return }
        giphyService.fetchGifImage(link: link) { data in
            completion(data)
        } failure: { [unowned self] error in
            view.showAlert(title: "Attention", message: error.localizedDescription)
        }
    }
    
    func openGifDetailsModule(gifObject: GifObject) {
        let module: GifDetailsModule = .init(gifObject: gifObject)
        module.present(from: view.viewController)
    }
    
    func switchCategory(with indexPath: IndexPath) {
        currentCategory = categories[indexPath.item]
        gifs = []
        view.removeData()
        pageOffset = 0
        fetchGifs()
    }
    
    // MARK: private methods
    
    private func calculateIndexPathsToReload() -> [IndexPath] {
        let paths: [IndexPath] = ((pageOffset * kGifLimit)..<(kGifLimit + pageOffset * kGifLimit)).map { IndexPath(item: $0, section: 0) }
        return paths
    }
}
