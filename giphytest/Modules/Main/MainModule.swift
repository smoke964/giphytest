//
//  MainModule.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

final class MainModule: ModuleProtocol {
    func present(from vc: UIViewController) {
        vc.present(view.viewController, animated: true)
    }
    
    let presenter: MainPresenter = .init()
    let view: MainViewProtocol
    
    init() {
        view = MainViewController()
        view.presenter = presenter
        
        let networkProvider: NetworkProviderProtocol = NetworkProvider()
        
        presenter.view = view
        presenter.giphyService = GiphyService(networkProvider: networkProvider)
    }
    
    func setAsRootView(to window: UIWindow) {
        let navigationController = UINavigationController(rootViewController: view.viewController)
        navigationController.modalPresentationStyle = .custom
        navigationController.navigationBar.alpha = 0
        window.rootViewController = navigationController
    }
}
