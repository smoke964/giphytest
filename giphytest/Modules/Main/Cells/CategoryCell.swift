//
//  CategoryCell.swift
//  giphytest
//
//  Created by Иван Грицак on 28.01.2023.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    private let categoryLabel: UILabel = {
        let label: UILabel = .init()
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    func setupCell(category: String, isActive: Bool) {
        contentView.addSubview(categoryLabel)

        layer.cornerRadius = 20

        backgroundColor = isActive ? .violetColor : .clear

        NSLayoutConstraint.activate([
            categoryLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            categoryLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])

        categoryLabel.text = category
    }
}
