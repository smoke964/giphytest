//
//  GifCell.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit

class GifCell: UICollectionViewCell {
    
    private var gifImageView: UIImageView = {
        let image: UIImageView = .init()
        image.layer.cornerRadius = 4
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    func setupCellLayout() {
        contentView.backgroundColor = .init(red: .random(in: 0..<1), green: .random(in: 0..<1), blue: .random(in: 0..<1), alpha: 1)
        contentView.layer.cornerRadius = 4
        contentView.createPlaceholderGradientAnimation()
        contentView.addSubview(gifImageView)
        NSLayoutConstraint.activate([
            gifImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            gifImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            gifImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            gifImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
    
    func setupCell(image: UIImage?) {
        gifImageView.image = image
        contentView.removeGradientAnimation()
        contentView.backgroundColor = .clear
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let targetSize: CGSize = .init(width: layoutAttributes.frame.width, height: 0)
        layoutAttributes.frame.size = contentView.systemLayoutSizeFitting(targetSize)
        return layoutAttributes
    }
    
    override func prepareForReuse() {
        gifImageView.image = nil
    }
}
