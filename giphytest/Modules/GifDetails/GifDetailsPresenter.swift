//
//  GifDetailsPresenter.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit
import Photos
import MessageUI

final class GifDetailsPresenter: NSObject, UINavigationControllerDelegate {
    weak var view: GifDetailsViewProtocol!
    var giphyService: GiphyServiceProtocol!
    
    private var gifObject: GifObject
    private var gifData: Data?
    
    private var controller: MFMessageComposeViewController?
    
    init(gifObject: GifObject) {
        self.gifObject = gifObject
    }
    
    func viewIsReady() {
        guard let fullsizeLink: String = gifObject.images["original"]?.url else { return }
        view.setCorrectButtonLabels(with: gifObject.type)
        giphyService.fetchGifImage(link: fullsizeLink) { [weak self] data in
            self?.view.setFullsizeGifImage(data: data)
            self?.gifData = data
        } failure: { [weak self] error in
            self?.view.showAlert(title: "Attention", message: error.localizedDescription)
        }
    }
    
    func copyToClipboard() {
        let clipboard: UIPasteboard = .general
        clipboard.string = gifObject.images["original"]?.url
        view.showOperationSuccessMessage(message: "Copied")
    }
    
    func saveGifToCameraRoll() {
        guard let gifData else { return }
        PHPhotoLibrary.shared().performChanges { [weak self] in
            PHAssetCreationRequest.forAsset().addResource(with: .photo, data: gifData, options: nil)
            DispatchQueue.main.async {
                self?.view.showOperationSuccessMessage(message: "Saved")
            }
        }
    }
    
    func share() {
        guard let gifData else { return }
        let activity: UIActivityViewController = .init(activityItems: [gifData], applicationActivities: nil)
        view.viewController.present(activity, animated: true)
    }
    
    func shareViaTwitter() {
        guard let link: String = gifObject.images["original"]?.url else {
            view.showAlert(title: "Attention", message: "Unable to share")
            return }
        let shareString: String = "https://twitter.com/intent/tweet?url=\(link)"
        guard let url: URL = .init(string: shareString) else {
            view.showAlert(title: "Attention", message: "Unable to share")
            return }
        UIApplication.shared.open(url)
    }
    
    func shareViaPinterest() {
        guard let link: String = gifObject.images["original"]?.url else {
            view.showAlert(title: "Attention", message: "Unable to share")
            return }
        let shareString: String = "https://www.pinterest.com/?next=/pin/create/bookmarklet/?media=\(link)"
        guard let url: URL = .init(string: shareString) else {
            view.showAlert(title: "Attention", message: "Unable to share")
            return }
        UIApplication.shared.open(url)
    }
    
    func shareViaMessages() {
        guard let link: String = gifObject.images["original"]?.url else {
            view.showAlert(title: "Attention", message: "Unable to share")
            return }
        controller = .init()
        controller?.body = link
        controller?.messageComposeDelegate = self
        view.viewController.present(controller!, animated: true) // freshly init'd
    }
}

extension GifDetailsPresenter: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .cancelled:
            controller.dismiss(animated: true)
        case .sent:
            view.showOperationSuccessMessage(message: "Gif Sent")
        case .failed:
            view.showAlert(title: "Attention", message: "Something went wrong")
        @unknown default:
            break
        }
    }

}
