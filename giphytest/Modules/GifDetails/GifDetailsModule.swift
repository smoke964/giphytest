//
//  GifDetailsModule.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit

final class GifDetailsModule: ModuleProtocol {
    let presenter: GifDetailsPresenter
    let view: GifDetailsViewProtocol
    
    init(gifObject: GifObject) {
        presenter = .init(gifObject: gifObject)
        view = GifDetailsViewController()
        
        presenter.view = view
        view.presenter = presenter
        
        let networkProvider: NetworkProviderProtocol = NetworkProvider()
        presenter.giphyService = GiphyService(networkProvider: networkProvider)
    }
    
    func present(from vc: UIViewController) {
        let navController: UINavigationController = .init(rootViewController: view.viewController)
        vc.present(navController, animated: true)
    }
}
