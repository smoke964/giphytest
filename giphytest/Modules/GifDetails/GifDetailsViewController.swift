//
//  GifDetailsViewController.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit

protocol GifDetailsViewProtocol: ViewProtocol {
    var presenter: GifDetailsPresenter! { get set }
    
    func setFullsizeGifImage(data: Data)
    func setCorrectButtonLabels(with type: GifObjectType)
}

final class GifDetailsViewController: UIViewController {
    
    var presenter: GifDetailsPresenter!
    
    // MARK: - UI
    
    private lazy var closeBarItem: UIBarButtonItem = {
        let item: UIBarButtonItem = .init(image: .init(named: "xmark"), style: .plain, target: self, action: #selector(closeView))
        item.tintColor = .white
        item.customView?.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()
    
    private lazy var shareBarItem: UIBarButtonItem = {
        let item: UIBarButtonItem = .init(image: .init(named: "square.and.arrow.up"), style: .plain, target: self, action: #selector(share))
        item.tintColor = .white
        item.customView?.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let indicator: UIActivityIndicatorView = .init(style: .white)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    private let gifImageView: UIImageView = {
        let view: UIImageView = .init(frame: .zero)
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var shareViaTwitterButton: UIButton = {
        let button: UIButton = .init()
        button.setImage(.init(named: "twitter"), for: .normal)
        button.addTarget(self, action: #selector(shareViaTwitter), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.widthAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var shareViaMessagesButton: UIButton = {
        let button: UIButton = .init()
        button.setImage(.init(named: "message"), for: .normal)
        button.addTarget(self, action: #selector(shareViaMessages), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.widthAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var shareViaPinterestButton: UIButton = {
        let button: UIButton = .init()
        button.setImage(.init(named: "pinterest"), for: .normal)
        button.addTarget(self, action: #selector(shareViaPinterest), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.widthAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var mediaShareStackView: UIStackView = {
        let stack: UIStackView = .init()
        stack.addArrangedSubview(shareViaTwitterButton)
        stack.addArrangedSubview(shareViaMessagesButton)
        stack.addArrangedSubview(shareViaPinterestButton)
        stack.spacing = 8
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.heightAnchor.constraint(equalToConstant: 48).isActive = true
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var copyGifLinkButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.addTarget(self, action: #selector(copyLink), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.setTitle("Copy GIF Link", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14, weight: .semibold)
        button.backgroundColor = .purpleColor
        button.layer.cornerRadius = 4
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var copyGifButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.addTarget(self, action: #selector(saveGifToCameraRoll), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.setTitle("Save GIF", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14, weight: .semibold)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 4
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    // MARK: - private methods
    
    @objc private func closeView() {
        dismiss(animated: true)
    }
    
    @objc private func share() {
        presenter.share()
    }
    
    @objc private func shareViaTwitter() {
        presenter.shareViaTwitter()
    }
    
    @objc private func shareViaMessages() {
        presenter.shareViaMessages()
    }
    
    @objc private func shareViaPinterest() {
        presenter.shareViaPinterest()
    }
    
    @objc private func copyLink() {
        presenter.copyToClipboard()
    }
    
    @objc private func saveGifToCameraRoll() {
        presenter.saveGifToCameraRoll()
    }
    
    private func setupView() {
        
        navigationItem.leftBarButtonItem = closeBarItem
        navigationItem.rightBarButtonItem = shareBarItem
        
        view.backgroundColor = .black
        view.addSubview(gifImageView)
        view.addSubview(mediaShareStackView)
        view.addSubview(copyGifLinkButton)
        view.addSubview(copyGifButton)
        view.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            gifImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            gifImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            gifImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            mediaShareStackView.topAnchor.constraint(greaterThanOrEqualTo: gifImageView.bottomAnchor),
            mediaShareStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            copyGifLinkButton.topAnchor.constraint(equalTo: mediaShareStackView.bottomAnchor, constant: 10),
            copyGifLinkButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            copyGifLinkButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            copyGifButton.topAnchor.constraint(equalTo: copyGifLinkButton.bottomAnchor, constant: 10),
            copyGifButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            copyGifButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            copyGifButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            activityIndicator.centerXAnchor.constraint(equalTo: gifImageView.centerXAnchor),
            activityIndicator.topAnchor.constraint(equalTo: view.topAnchor, constant: 200)
        ])
        
        activityIndicator.startAnimating()
    }
    
    override func viewDidLoad() {
        setupView()
        super.viewDidLoad()
        presenter.viewIsReady()
    }
}

extension GifDetailsViewController: GifDetailsViewProtocol {
    func setCorrectButtonLabels(with type: GifObjectType) {
        switch type {
        case .gif:
            copyGifButton.setTitle("Save GIF", for: .normal)
            copyGifLinkButton.setTitle("Copy GIF Link", for: .normal)
        case .sticker:
            fallthrough
        case .text:
            copyGifButton.setTitle("Save Sticker", for: .normal)
            copyGifLinkButton.setTitle("Copy Sticker Link", for: .normal)
        }
    }
    
    func setFullsizeGifImage(data: Data) {
        autoreleasepool {
            let image: UIImage? = .gifImageWithData(data)
            DispatchQueue.main.async { [weak self] in
                self?.gifImageView.image = image
                self?.activityIndicator.stopAnimating()
            }
        }
    }
}
