//
//  ViewProtocol.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

protocol ViewProtocol: AnyObject {
    var viewController: UIViewController { get }

    func showAlert(title: String, message: String)
    func showOperationSuccessMessage(message: String)
}

extension ViewProtocol where Self: UIViewController {
    var viewController: UIViewController {
        return self
    }
    
    func showAlert(title: String, message: String) {
        let alert: UIAlertController = .init(title: title, message: message, preferredStyle: .alert)
        let dismissAction: UIAlertAction = .init(title: "OK", style: .default) { [weak self] _ in
            self?.dismiss(animated: true)
        }
        alert.addAction(dismissAction)
        viewController.present(alert, animated: true)
    }
    func showOperationSuccessMessage(message: String) {
        let view: UIView = .init()
        view.alpha = 0
        view.backgroundColor = .lightGray.withAlphaComponent(0.65)
        let checkmark: UIImageView = .init(image: .init(named: "checkmark.circle.fill"))
        let label: UILabel = .init()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = message
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        label.textAlignment = .center
        checkmark.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.addSubview(checkmark)
        view.addSubview(label)
        self.view.addSubview(view)
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 120),
            view.widthAnchor.constraint(equalToConstant: 120),
            checkmark.heightAnchor.constraint(equalToConstant: 48),
            checkmark.widthAnchor.constraint(equalToConstant: 48),
            checkmark.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            checkmark.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -16),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
            label.topAnchor.constraint(equalTo: checkmark.bottomAnchor, constant: 4),
            view.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            view.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
        ])
        
        UIView.animate(withDuration: 0.3) {
            view.alpha = 1
        } completion: { isComplete in
            if isComplete {
                UIView.animate(withDuration: 0.3, delay: 1.3) {
                    view.alpha = 0
                } completion: { _ in
                    view.removeFromSuperview()
                }
            }
        }
    }
}
