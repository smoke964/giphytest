//
//  ModuleProtocol.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

protocol ModuleProtocol: AnyObject {
    func present(from vc: UIViewController)
}

