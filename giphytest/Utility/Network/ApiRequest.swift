//
//  ApiRequest.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

protocol ApiRequest: AnyObject {
    var fullURL: URL? { get }
    var params: [String: Any] { get set }
    
    var method: String? { get set }
    var body: () -> Data? { get }
    var asURLRequest: URLRequest? { get }
    var cachePolicy: URLRequest.CachePolicy? { get }
}

extension ApiRequest {
    var cachePolicy: URLRequest.CachePolicy? { nil }
    var body: () -> Data? { {return nil } }
    var params: [String: Any] { get {[:]} set {} }
    var asURLRequest: URLRequest? {
        guard let url = fullURL else { return nil }
        var req: URLRequest = .init(url: url)
        req.httpMethod = method ?? "POST"
        req.httpBody = body()
        if let cachePolicy {
            req.cachePolicy = cachePolicy
        }
        return req
    }
}
