//
//  NetworkProvider.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import UIKit

typealias ServiceFailureBlock = (ApiError) -> Void

protocol NetworkProviderProtocol: AnyObject {
    func send<M: Codable>(request: ApiRequest, success: @escaping (M?) -> Void, failure: @escaping ServiceFailureBlock)
    func send(request: ApiRequest, success: @escaping (Data, URLResponse) -> Void, failure: @escaping ServiceFailureBlock)
}

final class NetworkProvider: NetworkProviderProtocol {
    private let session: URLSession
    private let decoder: JSONDecoder

    init() {
        let conf: URLSessionConfiguration = .default
        conf.timeoutIntervalForResource = 60
        conf.timeoutIntervalForRequest = 60
        let session: URLSession = .init(configuration: conf)
        self.session = session
        decoder = .init()
    }
    
    func send(request: ApiRequest, success: @escaping (Data, URLResponse) -> Void, failure: @escaping ServiceFailureBlock) {
        guard let request: URLRequest = request.asURLRequest else {
            failure(ApiError(code: -101, message: "Unable to build URL request"))
            return
        }
        
        session.dataTask(with: request) { data, res, error in
            if let error {
                failure(ApiError(code: -102, message: error.localizedDescription))
                return
            }
            if let res = res as? HTTPURLResponse {
                switch HTTPCode(rawValue: res.statusCode) {
                case .ok:
                    break
                default:
                    failure(ApiError(code: res.statusCode, message: res.description))
                    return
                }
            }
            if let data, let res {
                success(data, res)
            }
        }.resume()
    }

    func send<M: Codable>(request: ApiRequest, success: @escaping (M?) -> Void, failure: @escaping ServiceFailureBlock) {
        guard let request: URLRequest = request.asURLRequest else {
            failure(ApiError(code: -101, message: "Unable to build URL request"))
            return
        }
        session.dataTask(with: request) { [self] data, res, error in

            if let error {
                failure(ApiError(code: -102, message: error.localizedDescription))
                return
            }
            if let res = res as? HTTPURLResponse {
                switch HTTPCode(rawValue: res.statusCode) {
                case .ok:
                    break
                default:
                    failure(ApiError(code: res.statusCode, message: res.description))
                    return
                }
            }
            if let data {
                do {
                    let d = try decoder.decode(M.self, from: data)
                    success(d)
                } catch let error {
                    failure(ApiError(code: -103, message: error.localizedDescription))
                }
            }
        }.resume()
    }
}
