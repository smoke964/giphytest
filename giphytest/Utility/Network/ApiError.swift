//
//  ApiError.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

struct ApiError: Error {
    var code: Int
    var message: String
}
