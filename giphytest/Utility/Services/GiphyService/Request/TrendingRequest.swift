//
//  TrendingRequest.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

enum Rating: String, Codable {
    case g
    case pg
    case pg13 = "pg-13"
    case r
}

final class TrendingRequest: ApiRequest {
    var params: [String : Any] = [:]
    
    var fullURL: URL?
    var method: String? = "GET"
    
    init(type: GiphyCategory, limit: Int = 25, offset: Int = 0, rating: Rating?) {
        params["api_key"] = Constants.apiKey
        params["limit"] = limit
        params["rating"] = rating?.rawValue
        params["offset"] = offset
        let urlParams: String = "?\(params.map { "\($0.key)=\($0.value)" }.joined(separator: "&"))"
        fullURL = URL(string: Constants.baseApiUrl + type.rawValue + "/trending" + urlParams)
    }
}
