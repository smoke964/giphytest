//
//  FetchGifImageRequest.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import Foundation

final class FetchGifImageRequest: ApiRequest {
    var fullURL: URL?
    var method: String? = "GET"

    init(from string: String) {
        fullURL = URL(string: string)
    }
}
