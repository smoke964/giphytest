//
//  GiphyService.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

typealias FetchTrendingGifsSuccessBlock = (TrendingResponse) -> Void
typealias FetchGifImageSuccessBlock = (Data) -> Void

enum GiphyCategory: String, CaseIterable {
    case gifs = "gifs"
    case stickers = "stickers"
}

protocol GiphyServiceProtocol {
    func fetchTrendingGifs(type: GiphyCategory,
                           rating: Rating?,
                           limit: Int,
                           offset: Int,
                           success: @escaping FetchTrendingGifsSuccessBlock,
                           failure: @escaping ServiceFailureBlock)
    
    func fetchGifImage(link: String,
                       success: @escaping FetchGifImageSuccessBlock,
                       failure: @escaping ServiceFailureBlock)
}

final class GiphyService: BaseService {
}

extension GiphyService: GiphyServiceProtocol {
    func fetchTrendingGifs(type: GiphyCategory, rating: Rating?, limit: Int, offset: Int, success: @escaping FetchTrendingGifsSuccessBlock, failure: @escaping ServiceFailureBlock) {
        let request: TrendingRequest = .init(type: type, limit: limit, offset: offset, rating: rating)
        load(networkProvider: networkProvider, request: request, success: success, failure: failure)
    }
    
    func fetchGifImage(link: String, success: @escaping FetchGifImageSuccessBlock, failure: @escaping ServiceFailureBlock) {
        let request: FetchGifImageRequest = .init(from: link)
        load(networkProvider: networkProvider, request: request, success: success, failure: failure)
    }
}
