//
//  TrendingResponse.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

struct TrendingResponse: Codable {
    let data: [GifObject]
    let pagination: Pagination
    let meta: Meta
}

struct Pagination: Codable {
    let offset: Int
    let totalCount: Int
    let count: Int
    
    enum CodingKeys: String, CodingKey {
        case offset
        case totalCount = "total_count"
        case count
    }
}

struct Meta: Codable {
    let msg: String
    let status: Int
    let responseId: String
    
    enum CodingKeys: String, CodingKey {
        case msg
        case status
        case responseId = "response_id"
    }
}
