//
//  BaseService.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

class BaseService {
    
    private(set) var networkProvider: NetworkProviderProtocol
    let cache: URLCache = .shared
    
    init(networkProvider: NetworkProviderProtocol) {
        self.networkProvider = networkProvider
    }
    
    func load(networkProvider: NetworkProviderProtocol,
                 request: ApiRequest,
                 success: @escaping ((Data) -> ()),
                 failure: @escaping ServiceFailureBlock
    ) {
        guard let urlRequest = request.asURLRequest else {
            failure(ApiError(code: -101, message: "Unable to build URL request"))
            return
        }
        
        if let data = cache.cachedResponse(for: urlRequest)?.data {
            success(data)
            return
        }
        networkProvider.send(request: request, success: { [weak self] data, response in
            let cachedData: CachedURLResponse = .init(response: response, data: data)
            self?.cache.storeCachedResponse(cachedData, for: urlRequest)
            success(data)
        }, failure: failure)
    }
    
    func load<M: Codable>(networkProvider: NetworkProviderProtocol,
                 request: ApiRequest,
                 success: @escaping ((M) -> ()),
                 failure: @escaping ServiceFailureBlock
    ) {
        networkProvider.send(request: request, success: { (data: M?) in
            if let model = data {
                DispatchQueue.main.async {
                    success(model)
                }
            } else {
                DispatchQueue.main.async {
                    failure(ApiError(code: -100, message: Constants.Errors.internalError))
                }
            }
        }) { error in
            DispatchQueue.main.async {
                failure(error)
            }
        }
    }

}
