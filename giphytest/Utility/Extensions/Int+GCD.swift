//
//  Int+GCD.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import Foundation

extension Int {
    static func gcd(_ first: Int, _ second: Int) -> Self {
        guard first > 0, second > 0 else { return 0 }
        var first = first
        var second = second
        
        var temp: Int
        if first < second {
            temp = first
            first = second
            second = temp
        }

        var rest: Int
        while true {
            rest = first % second

            if rest == 0 {
                return second
            } else {
                first = second
                second = rest
            }
        }
    }
    
    static func arrayGcd(_ array: [Int]) -> Self {
        if array.isEmpty {
            return 1
        } else {
           return array.reduce(array[0], { gcd($0, $1) })
        }
    }
}
