//
//  UIScrollView+.swift
//  giphytest
//
//  Created by Иван Грицак on 28.01.2023.
//

import UIKit


extension UIScrollView {
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
}
