//
//  String+Size.swift
//  giphytest
//
//  Created by Иван Грицак on 28.01.2023.
//

import UIKit

extension String {
    func getSize() -> CGSize {
        let str: NSString = self as NSString
        let font: UIFont = .systemFont(ofSize: 17, weight: .semibold)
        let attrs: [NSAttributedString.Key: Any] = [.font: font]
        let bbox = str.size(withAttributes: attrs)
        return bbox
    }
}
