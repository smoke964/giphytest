//
//  UIColor+App.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit

extension UIColor {
    static var purpleColor: UIColor { #colorLiteral(red: 0.0, green: 0.278, blue: 0.671, alpha: 1.0) }
    static var violetColor: UIColor { #colorLiteral(red: 0.541, green: 0.169, blue: 0.886, alpha: 1.0) }
}
