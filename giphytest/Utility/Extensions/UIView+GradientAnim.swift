//
//  UIView+GradientAnim.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import UIKit

extension UIView {
    func createPlaceholderGradientAnimation() {
        let gradient = CAGradientLayer()
        
        let gradientHighlightRatio = 1.5
        let animationDuration = 2.0
        let startLocations = [NSNumber(value: -gradientHighlightRatio), NSNumber(value: -gradientHighlightRatio / 2), 0.0]
        let endLocations = [1, NSNumber(value: 1 + (gradientHighlightRatio / 2)), NSNumber(value: 1 + gradientHighlightRatio)]
        
        gradient.colors = [
            UIColor(white: 1, alpha: 0.1).cgColor,
            UIColor(white: 1, alpha: 0.8).cgColor,
            UIColor(white: 1, alpha: 0.1).cgColor
        ]
        
        gradient.frame = self.bounds
        gradient.borderWidth = self.layer.borderWidth
        gradient.cornerRadius = self.layer.cornerRadius
        gradient.startPoint = CGPoint(x: -0.5, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.5, y: 0.5)
        gradient.locations = startLocations
        
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        gradientAnimation.fromValue = startLocations
        gradientAnimation.toValue = endLocations
        gradientAnimation.duration = animationDuration
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = animationDuration
        animationGroup.repeatCount = .infinity
        animationGroup.isRemovedOnCompletion = false
        animationGroup.animations = [gradientAnimation]
        
        gradient.removeAnimation(forKey: "locations")
        gradient.add(animationGroup, forKey: "locations")
        
        self.layer.addSublayer(gradient)
    }
    
    func removeGradientAnimation() {
        self.layer.sublayers?.first(where: { $0 is CAGradientLayer })?.removeFromSuperlayer()
    }
}
