//
//  Collection+.swift
//  giphytest
//
//  Created by Иван Грицак on 27.01.2023.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
