//
//  Comparable+Clamped.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

extension Comparable {
    func clamped(to limits: ClosedRange<Self>) -> Self {
        min(max(self, limits.lowerBound), limits.upperBound)
    }
}
