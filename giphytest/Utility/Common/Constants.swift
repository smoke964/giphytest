//
//  Constants.swift
//  giphytest
//
//  Created by Иван Грицак on 26.01.2023.
//

import Foundation

public struct Constants {
    static let apiKey: String = "kfjgiSqeA34X2a0UGCgAAUkvZVCYLdI8"
    static let baseApiUrl: String = "https://api.giphy.com/v1/"
    
    struct Errors {
        static let internalError: String = "An internal error occured"
    }
}

enum HTTPCode: Int {
    case ok = 200
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case uriTooLong = 414
    case tooManyRequests = 429
}
